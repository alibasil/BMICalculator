﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BMICalculator
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private Int32 systemSelect_SelectionChanged()
		{
            int selection = 0;
            if (systemSelect.SelectedItem == imperial)
                selection = 1;
            else if (systemSelect.SelectedItem == metric)
                selection = 2;

            return selection;
		}
		private void heightBox_TextChanged(object sender, TextChangedEventArgs e)
		{
		}
		private void weightBox_TextChanged(object sender, TextChangedEventArgs e)
		{
		}
		private double heightBox_TextChanged2()
		{
			double height;

			if (double.TryParse(heightBox.Text, out height))
				height = Convert.ToDouble(heightBox.Text);

			else
				MessageBox.Show("please enter numbers only");


			return height;
		}

		private double weightBox_TextChanged2()
		{
			double weight;
			if (double.TryParse(weightBox.Text, out weight))
				weight = Convert.ToDouble(weightBox.Text);
			else
				MessageBox.Show("please enter numbers only");
			

			return weight;
		}

        private void submitBtn_Click(object sender, RoutedEventArgs e)
        {
            double height;
            double weight;
            double bmi = 0;
            int selection = 0;
            height = heightBox_TextChanged2();
            weight = weightBox_TextChanged2();
            selection = systemSelect_SelectionChanged();

            if (selection == 1)
                bmi = weight / (float)(height * height) * 703;
            else if (selection == 2)
                bmi = weight / (float)(height * height);
            else if (selection == 0)
                MessageBox.Show("Please select a unit system");

            if (bmi != 0)
                MessageBox.Show(bmi.ToString());

        }
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }
 

        private void RadioEnglish_Checked(object sender, RoutedEventArgs e)
        {
            LabelInstruction.Content = "Select your unit:";
            heightLabel.Content = "Enter your height:";
            weightLabel.Content = "Enter your weight:";
            imperial.Content = "Imerial (lbs and in)";
            metric.Content = "Metric (kg and m)";

        }

        private void RadioFrench_Checked(object sender, RoutedEventArgs e)
        {
            LabelInstruction.Content = "Sélectionnez votre unité:";
            heightLabel.Content = "Entrez votre hauteur:";
            weightLabel.Content = "Entrez votre poids:";
            imperial.Content = "Impérial (lbs et in)";
            metric.Content = "Métrique (kg et m)";
        }
    }

    
    
}
